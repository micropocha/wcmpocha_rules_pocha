(async () => {
  class wcmPochaRulesPocha extends HTMLElement {
    constructor() {  
        super();
    }

    get name () {
      return "pocha"  
    }

    get activeRoundFlag (){
      return null;
    }

    set activeRoundFlag (flag){}
    
    buttons (lastScore, lastPartial) {
      var scoreforHits = (hits) => {
        if (hits >= 0) return hits * 5 +10
          else return hits * 5
      }
      var caption = (hits, deltaPoints) => {
        return `(${hits}) ${deltaPoints} => ${lastScore + deltaPoints}`
      } 
      var button = (hits) => {
        return {
          value: hits,
          caption : caption (hits, scoreforHits(hits))
        }
      };
      if(  lastPartial !== undefined && lastPartial !== null ){
        return [button(lastPartial-1), button(lastPartial+1)];
      }
      else{
        return [button(-1 ), button( 0 )];
      }
    }
    
    get roundFlags (){
      return [];
    }

    dealAmount ( round, playerAmount ){
      if(playerAmount == 4 ) {
        if (round < 10) return round;
        else if (round >=10 && round <14) return 10;
        else if (round >=14 && round <23) return 23 - round;
        else return 10;
      }
      else if(playerAmount == 3 ) {
        if (round < 9) return round;
        else if (round < (9+playerAmount)) return 9;
        else if (round < 9 + playerAmount + 8) return 20 - round;
        else return 9;
      }
      else return null;      
    }
  }
 
  customElements.define('wcmpocha-rules-pocha', wcmPochaRulesPocha);
})();