Description
==================================

this component contains the specific rules of a game of kind pocha

## attributes (html):

## properties (js):

- name: display name of the rule
- buttons: array  of display name for buttons (initially 2) containing value and caption
- dealAmount: number of cards to deal 
- roundFlags: array of roundOptions
- activeRoundFlag: to active roundflag 

## events

## listens


Run
=======================================

## run first time

```bash
npm install 
npm start
```

## test

```bash
npm test
```